﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public GameObject bronzeCoin;
    public GameObject silverCoin;
    public GameObject goldCoin;
    public GameObject enemy;
    public GameObject health;

    private float plataformSpeed = 0.75f;
    private bool isDirecctionFlipped;
    private bool isMoving;
    private bool hasCoin;
    private bool hasHealth;

    private Vector3 initialPosition;
    public int coinValue;

    private void Start()
    {
        isMoving = GetRandomBoolean();
        isDirecctionFlipped = GetRandomBoolean();
        hasCoin = GetRandomBoolean();
        hasHealth = GetRandomBoolean();
        coinValue = Random.Range(0, 3);
        initialPosition = transform.position;

        //Generate Coin
        if (!isMoving)
        {
            if (hasCoin)
            {
                switch (coinValue)
                {
                    case 0:
                        Instantiate(goldCoin, initialPosition + new Vector3(Random.Range(-2, 2), Random.Range(2, 5), 0), Quaternion.identity);
                        break;
                    case 1:
                        Instantiate(silverCoin, initialPosition + new Vector3(Random.Range(-2, 2), Random.Range(2, 5), 0), Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(bronzeCoin, initialPosition + new Vector3(Random.Range(-2, 2), Random.Range(2, 5), 0), Quaternion.identity);
                        break;
                }
            }
            else if (hasHealth)
            {
                Instantiate(health, initialPosition + new Vector3(Random.Range(-2, 2), Random.Range(2, 3), 0), Quaternion.identity);
            }else
            {
                Instantiate(enemy, initialPosition + new Vector3(0, 0.9f, 0), Quaternion.identity);
            }
        }
    }

    private void Update()
    {
        if (isMoving)
            movePlataform();
    }

    private void movePlataform()
    {
        Vector2 position = transform.position;

        if (position.x > 7 || position.x < -7)
        {
            isDirecctionFlipped = !isDirecctionFlipped;
        }

        transform.position += (isDirecctionFlipped) ? new Vector3(plataformSpeed * Time.fixedDeltaTime, 0, 0) : new Vector3(-plataformSpeed * Time.fixedDeltaTime, 0, 0);
    }

    private bool GetRandomBoolean()
    {
        return Random.Range(0, 100) % 2 == 0;
    }
}
