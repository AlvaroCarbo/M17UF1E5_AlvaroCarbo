﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonBulletController : MonoBehaviour
{
    //private bool isFlippedDireccion = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= new Vector3(1, 0, 0) * Time.fixedDeltaTime;

        if (transform.position.x < -10)
        {
            transform.position += new Vector3(20, 0, 0);
        }
    }
}
