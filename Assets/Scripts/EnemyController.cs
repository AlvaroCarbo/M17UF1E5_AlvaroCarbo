﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public LayerMask groundLayerMask;
    public GameObject player;

    public Vector3 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        initialPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = transform.position;
        float temp = pos.y;

        if (pos.x > initialPos.x - 2 && pos.x < initialPos.x + 2)
        {
            if ((player.transform.position.y <= pos.y && player.transform.position.y + 1f >= pos.y) && (player.transform.position.x <= initialPos.x + 3 && player.transform.position.y >= initialPos.x - 3))
            {
                float tempDirection = pos.x;
                pos = Vector3.MoveTowards(transform.position, player.transform.position, 0.5f * Time.fixedDeltaTime);
                if (pos.x > tempDirection)
                    gameObject.GetComponent<SpriteRenderer>().flipX = true;
                else if (pos.x < tempDirection)
                    gameObject.GetComponent<SpriteRenderer>().flipX = false;
                pos.y = temp;
            }
        }else if (pos.x > initialPos.x + 2)
        {
            pos.x -= 0.6f * Time.fixedDeltaTime;
        }else if (pos.x < initialPos.x - 2)
        {
            pos.x += 0.6f * Time.fixedDeltaTime;
        }

        transform.position = pos;
    }
}
