﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public GameObject Player;
    
    public GameObject EndGameUI;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }

    void Start()
    {
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        PlayerDeath();
    }

    void PlayerDeath()
    {
        if ((Player.GetComponent<Player>().health <= 0) || (Player.GetComponent<Transform>().position.y < Player.GetComponent<Player>().m_MainCamera.transform.position.y - 18))
        {
            EndGameUI.SetActive(true);
            Player.GetComponent<Player>().isDead = true;
        }
    }
}
