﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndUIController : MonoBehaviour
{
    [SerializeField]
    private Button restartButton;
    [SerializeField]
    private Text finalScoreText;
    [SerializeField]
    private UIController uiController;
    [SerializeField]
    private Player player;

    void Awake()
    {
        gameObject.SetActive(false);
        uiController = GameObject.Find("Canvas").GetComponent<UIController>();
        player = GameObject.Find("Player").GetComponent<Player>();
    }
    private void Start()
    {
        restartButton.onClick.AddListener(() => SceneManager.LoadScene(0));
    }

    private void Update()
    {
        finalScoreText.text = $"Score:\n{uiController.score} m\n\nEnemies Killed:\n{player.killedEnemies}";
    }
}
