﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    public Vector2 movementSpeed;
    public Vector2 movementDirection;
    public float gravity = -400f;
    public float jumpSpeed = 12f;

    public bool isJumping;
    public bool isMoving;
    public bool isDead;
    public bool isGrounded = true;
    public Vector2 groundPos;
    public Animator animator;


    public int coins;
    public int health = 1;
    public float maxHeight;


    public LayerMask groundLayerMask;
    public LayerMask coinLayerMask;
    public LayerMask silverCoinLayerMask;
    public LayerMask bronzeCoinLayerMask;
    public LayerMask healthLayerMask;
    public LayerMask enemyLayerMask;

    public Camera m_MainCamera;

    public int killedEnemies;

    void Start()
    {
        m_MainCamera = Camera.main;

        maxHeight = (int)transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        isMoving = false;
        movementSpeed.x = 0f;

        //Horizontal Movement
        if (Input.GetKey(KeyCode.RightArrow))
        {
            spriteRenderer.flipX = false;
            isMoving = true;
            movementSpeed.x = 8f;
            movementDirection.x = 1;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spriteRenderer.flipX = true;
            isMoving = true;
            movementSpeed.x = 8f;
            movementDirection.x = -1;
        }

        //Vertical Movement
        if (isGrounded)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                isJumping = true;
                isGrounded = false;
                movementSpeed.y = jumpSpeed;
                movementDirection.y = 1;
                groundPos.y = -10f;
            }
        }
        else
        {
            if (isJumping)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    isJumping = false;
                    isGrounded = false;
                    movementSpeed.y = jumpSpeed;
                    movementDirection.y = 1;
                    groundPos.y = -10f;
                }
            }
        }
    }

    private void FixedUpdate()
    {
        Vector2 pos = transform.position;
        var temp = transform.position;

        //Ground RaycastHit declaration
        Vector2 groundRayOrigin = new Vector2(pos.x, pos.y + 1);
        Vector2 groundRayDirection = Vector2.down;
        float rayDistance = 50 * Time.fixedDeltaTime;
        float rayDistanceGround = 25 * Time.fixedDeltaTime;

        Vector2 groundRayOriginRight = new Vector2(pos.x + 0.75f, pos.y + 0.50f);
        Vector2 groundRayOriginLeft = new Vector2(pos.x - 0.75f, pos.y + 0.50f);
        RaycastHit2D groundHitRight = Physics2D.Raycast(groundRayOriginRight, groundRayDirection, rayDistanceGround, groundLayerMask);
        RaycastHit2D groundHitLeft = Physics2D.Raycast(groundRayOriginLeft, groundRayDirection, rayDistanceGround, groundLayerMask);

        Debug.DrawRay(groundRayOriginRight, groundRayDirection * rayDistanceGround, Color.red);
        Debug.DrawRay(groundRayOriginLeft, groundRayDirection * rayDistanceGround, Color.red);
        Debug.DrawRay(groundRayOrigin, groundRayDirection * rayDistance, Color.red);

        //Ground RaycastHit collider check
        if (groundHitRight.collider != null)
            CheckGroundCollider(groundHitRight.collider);
        else if (groundHitLeft.collider != null)
            CheckGroundCollider(groundHitLeft.collider);
        else
        {
            isGrounded = false;
            groundPos.y = m_MainCamera.transform.position.y - 100;
        }

        //Coin RaycastHit declaration
        Vector2 coinRayOrigin = new Vector2(pos.x, pos.y + 1);

        RaycastHit2D coinHit2D = Physics2D.Raycast(coinRayOrigin, Vector2.up, rayDistance, coinLayerMask);
        RaycastHit2D silverCoinHit2D = Physics2D.Raycast(coinRayOrigin, Vector2.up, rayDistance, silverCoinLayerMask);
        RaycastHit2D bronzeCoinHit2D = Physics2D.Raycast(coinRayOrigin, Vector2.up, rayDistance, bronzeCoinLayerMask);

        Debug.DrawRay(coinRayOrigin, Vector2.up * rayDistance, Color.green);

        //Coin RaycastHit collider comprovation
        CheckCoinCollider(coinHit2D.collider, 10);
        CheckCoinCollider(silverCoinHit2D.collider, 5);
        CheckCoinCollider(bronzeCoinHit2D.collider, 1);

        //Health RaycastHit declaration and collider check
        RaycastHit2D healthHit2D = Physics2D.Raycast(coinRayOrigin, Vector2.up, rayDistance, healthLayerMask);

        if (health < 3)
        {
            if (healthHit2D.collider != null)
            {
                //Debug.Log($"{healthHit2D.collider} hit");

                health += 1;

                Destroy(healthHit2D.collider.gameObject);
            }
        }


        //Enemy RaycastHit declaration and collider check
        RaycastHit2D enenmyHit2DLeft = Physics2D.Raycast(coinRayOrigin, Vector2.left, rayDistance, enemyLayerMask);
        RaycastHit2D enenmyHit2DRight = Physics2D.Raycast(coinRayOrigin, Vector2.right, rayDistance, enemyLayerMask);

        Debug.DrawRay(coinRayOrigin, Vector2.left * rayDistance, Color.cyan);
        Debug.DrawRay(coinRayOrigin, Vector2.right * rayDistance, Color.cyan);

        CheckEnemyCollider(enenmyHit2DRight.collider);
        CheckEnemyCollider(enenmyHit2DLeft.collider);

        //Horizontal Movement
        if (isGrounded)
        {
            if (isMoving)
            {
                if (pos.x < groundPos.x + 3.5 && pos.x > groundPos.x - 3.5)
                    pos.x += movementDirection.x * movementSpeed.x * Time.fixedDeltaTime;
                else
                    isGrounded = false;
            }

        }

        //Vertical Movement
        else if (!isGrounded)
        {
            if (groundPos.y <= pos.y)
            {
                movementDirection.y = (movementSpeed.y > 0) ? 1 : -1;
                pos += new Vector2(movementDirection.x * movementSpeed.x * Time.fixedDeltaTime, movementDirection.y * Mathf.Abs(movementSpeed.y) * Time.fixedDeltaTime);
                movementSpeed.y += gravity * Time.fixedDeltaTime;
                isGrounded = false;
            }
            else
            {
                pos.y = groundPos.y;
                isGrounded = true;
                movementSpeed.y = 0f;
                movementDirection.y = 0;
            }
        }

        //Camera Control
        if (pos.x > m_MainCamera.orthographicSize * m_MainCamera.aspect - 1 || -pos.x > m_MainCamera.orthographicSize * m_MainCamera.aspect - 1)
            pos.x = temp.x;

        if (pos.y >= 0.5f)
            m_MainCamera.transform.position = new Vector3(0, pos.y, -10);

        if (pos.y > maxHeight)
            maxHeight = pos.y;

        if (pos.y <= maxHeight)
            m_MainCamera.transform.position = new Vector3(0, maxHeight, -10);


        //Animation Control
        animator.SetFloat("Speed Y", movementSpeed.y);
        animator.SetFloat("Speed X", movementSpeed.x);
        animator.SetBool("isGrounded", isGrounded);

        //Position Control
        transform.position = pos;
    }

    private void CheckGroundCollider(Collider2D collider)
    {
        if (collider != null && movementDirection.y != 1)
        {
            //Debug.Log($"{collider.bounds.center} hit");

            groundPos = collider.bounds.center;
            if (!isJumping)
            {
                isGrounded = true;
                movementSpeed.y = 0f;
            }
        }
    }

    private void CheckCoinCollider(Collider2D collider, int coinValue)
    {
        if (collider != null)
        {
            //Debug.Log($"{collider} hit");

            coins += coinValue;

            Destroy(collider.gameObject);
        }
    }
    
    private void CheckEnemyCollider(Collider2D collider)
    {
        if (collider != null)
        {
            //Debug.Log($"{collider} hit");

            health -= 1;

            Destroy(collider.gameObject);

            killedEnemies++;
        }
    }
}
