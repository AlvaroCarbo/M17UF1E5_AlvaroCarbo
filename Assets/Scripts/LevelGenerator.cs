﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] grounds;
    public Player player;
    public Vector3 position;
    // Start is called before the first frame update
    void Start()
    {

        position.z = 0;

        for (int i = 0; i < 200; i++)
        {
            RandomPosition(i);
            int randomPlataform = (int)Random.Range(0f, 2f);
            Instantiate(grounds[randomPlataform], position, Quaternion.identity);
        }
    }

    void RandomPosition(int i)
    {
        position.x = Random.Range(-7f, 7f);
        position.y = 6 * i;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
