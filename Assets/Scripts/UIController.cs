﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject player;
    public Text heightText;
    public Text coinsText;
    public Text healthText;
    public int score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int temp = (int)player.GetComponent<Transform>().position.y + 6;
        if (temp > score && temp > 0) 
            score = temp;
        
        heightText.text =  $"Score: {score}";
        coinsText.text =  (player.GetComponent<Player>().coins).ToString();
        healthText.text =  (player.GetComponent<Player>().health).ToString();
    }
}
